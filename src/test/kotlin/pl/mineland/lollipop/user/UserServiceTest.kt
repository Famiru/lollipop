package pl.mineland.lollipop.user

import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.context.web.WebAppConfiguration
import pl.mineland.lollipop.user.attribute.AttributeMapper
import java.time.LocalDate
import java.util.*
import javax.transaction.Transactional

@Transactional
@RunWith(SpringRunner::class)
@SpringBootTest
@WebAppConfiguration
class UserServiceTest {

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var userRepository: UserRepository

    @Test
    fun createUserTest() {
        userService.create(UserDto("TEST_EMAIL", "TEST_NICK", "TEST_PASSWORD"))
        assertNotNull("Doesn't exist", userService.getUserByEmail("TEST_EMAIL"))
    }

    @Test
    fun repositoryUserTest() {
        val testUser = userRepository.save(User("TEST_EMAIL", "TEST_NICK", "TEST_PASSWORD"))
        assertNotNull("Doesn't exist", userRepository.findById(testUser.id))
        assertNotNull("Doesn't exist", userRepository.findByEmail("TEST_EMAIL"))
        assertEquals(testUser.id, userRepository.findUserByEmail(testUser.email).id)
        assertEquals(8, testUser.attribute.toInt())
    }

    @Test
    fun stringToIntArrayTest() {
        val testUser = userRepository.save(User(UUID.fromString("123e4567-e89b-12d3-a456-426655440000"),
                "TEST_EMAIL", "TEST_NICK", "TEST_PASSWORD", LocalDate.now(), "5,5,5,5,5,5,5,5"))
        assertNotNull("Doesn't exist", userRepository.findById(testUser.id))
        println(AttributeMapper.stringToList(testUser.attribute))
    }
}
