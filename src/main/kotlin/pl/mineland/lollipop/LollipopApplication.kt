package pl.mineland.lollipop

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class LollipopApplication

fun main(args: Array<String>) {
	runApplication<LollipopApplication>(*args)
}
