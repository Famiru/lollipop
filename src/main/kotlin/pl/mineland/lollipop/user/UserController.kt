package pl.mineland.lollipop.user

import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@CrossOrigin
@RequestMapping("/parasite")
class UserController(@Autowired val userService: UserService) {
    /**
     * Get the user object with the selected id.
     *
     * @param id the user identifier
     * @return the request user object
     */
    @GetMapping( produces = ["application/json"])
    @ApiOperation("Get user by id.")
    @ApiResponses(
            ApiResponse(code = 200, message = "When user with given id exists.", response = UserDto::class),
            ApiResponse(code = 404, message = "When user with given id not exists.")
    )
    fun get(@ApiParam("The user identifier.") @RequestParam("id") id: UUID): ResponseEntity<UserDto> {
        return userService.get(id).map { ResponseEntity.ok(it) }.orElse(ResponseEntity.notFound().build())
    }

    @PostMapping(consumes = ["application/json"], produces = ["application/json"])
    @ApiOperation("Add new user.")
    @ApiResponses(
            ApiResponse(code = 200, message = "When user is successful created.", response = UserDto::class),
            ApiResponse(code = 400, message = "When user already exists or the request is malformed.")
    )
    // TODO @PreAuthorize("hasAuthority('admin')")
    fun create(@ApiParam("The user to save in database.")
               @RequestBody userDto: UserDto): ResponseEntity<UserDto> {
        return userService.create(userDto).map { ResponseEntity.ok(it) }.orElse(ResponseEntity.badRequest().build())
    }

    @PutMapping(consumes = ["application/json"], produces = ["application/json"])
    @ApiOperation("Update user data.")
    @ApiResponses(
            ApiResponse(code = 200, message = "When user is successful updated.", response = UserDto::class),
            ApiResponse(code = 400, message = "When user not exists or the request is malformed.")
    )
    // TODO @PreAuthorize("hasAuthority('admin')")
    fun update(@ApiParam("The user to modify.")
               @RequestBody userDto: UserDto): ResponseEntity<UserDto> {
        return userService.update(userDto).map { ResponseEntity.ok(it) }.orElse(ResponseEntity.badRequest().build())
    }

    @DeleteMapping
    @ApiOperation("Delete user by id.")
    @ApiResponses(
            ApiResponse(code = 200, message = "When user is successful deleted."),
            ApiResponse(code = 404, message = "When user with given id is not exists.")
    )
    // TODO @PreAuthorize("hasAuthority('admin')")
    fun delete(@ApiParam("The user identifier.") @RequestParam("id") id: UUID): ResponseEntity<Void> {
        return if (userService.get(id).isPresent) {
            userService.delete(id)
            ResponseEntity.ok().build()
        } else {
            ResponseEntity.badRequest().build()
        }
    }

    @GetMapping(value = ["/showAll"], produces = ["application/json"])
    @ApiOperation("Display all users.")
    @ApiResponses(
            ApiResponse(code = 200, message = "Always, the return value could be empty though.", response = List::class)
    )
    fun showAll(): List<UserDto> {
        return userService.listAll()
    }

}
