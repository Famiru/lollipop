package pl.mineland.lollipop.user
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import pl.mineland.lollipop.user.attribute.*
import java.time.LocalDate
import java.util.*

@ApiModel(description = "Represents User.")
class UserDto {

    @ApiModelProperty(value = "User identifier.")
    var id: UUID?

    @ApiModelProperty(value = "User email.")
    var email: String

    @ApiModelProperty(value = "User nick.")
    var nick: String

    @ApiModelProperty(value = "User password.")
    var password: String

    @ApiModelProperty(value = "User additional date.")
    var additionalDate: LocalDate

    @ApiModelProperty(value = "User attributes.")
    var attributeDto: AttributeDto

    /**
     * Create instance of the UserDto object used for frontend communication.
     */
    @JsonCreator
    constructor(
            @JsonProperty("id") id: UUID?,
            @JsonProperty("email") email: String,
            @JsonProperty("nick") nick: String,
            @JsonProperty("password") password: String,
            @JsonProperty("additional_date") additionalDate: LocalDate,
            @JsonProperty("attribute") attributeDto: AttributeDto) {
        this.id = id
        this.email = email
        this.nick = nick
        this.password = password
        this.additionalDate = additionalDate
        this.attributeDto = attributeDto
    }

    /**
     * Secondary UserDto constructor.
     */
    constructor(email: String, nick: String, password: String) {
        this.id = UUID.randomUUID()
        this.email = email
        this.nick = nick
        this.password = password
        this.additionalDate = LocalDate.now()
        this.attributeDto = AttributeDto(Strength.NORMAL,Perception.NORMAL,Endurance.NORMAL,Charisma.NORMAL,Intelligence.NORMAL,Ability.NORMAL,Luck.NORMAL,5)
    }

}
