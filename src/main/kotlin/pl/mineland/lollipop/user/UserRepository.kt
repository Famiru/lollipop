package pl.mineland.lollipop.user

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserRepository: JpaRepository<User, UUID> {

    @Query(value = "SELECT * FROM parasite WHERE email ILIKE :email", nativeQuery = true)
    fun findUserByEmail(@Param("email") email: String): User

    fun findByEmail(email: String): User
    fun findByNick(nick: String): User
}
