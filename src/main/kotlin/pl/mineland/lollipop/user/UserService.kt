package pl.mineland.lollipop.user

import java.util.*

interface UserService {
    fun get(id: UUID): Optional<UserDto>
    fun create(newsDto: UserDto): Optional<UserDto>
    fun update(newsDto: UserDto): Optional<UserDto>
    fun delete(id: UUID)
    fun listAll(): List<UserDto>
    fun getUserByEmail(email: String): UserDto
    fun getUserByNick(nick: String): UserDto
}
