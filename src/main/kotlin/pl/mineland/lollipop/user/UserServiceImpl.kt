package pl.mineland.lollipop.user

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class UserServiceImpl(@Autowired val userRepository: UserRepository): UserService {

    override fun get(id: UUID): Optional<UserDto> {
        val result = userRepository.findById(id)
        return if(result.isPresent) {
            Optional.ofNullable(UserMapper.toDto(result.get()))
        } else {
            Optional.empty()
        }
    }

    override fun create(userDto: UserDto): Optional<UserDto> {
        return if (userDto.id != null && userRepository.findById(userDto.id!!).isPresent) {
            Optional.empty()
        } else {
            val save = userRepository.save(UserMapper.fromDto(userDto))
            Optional.ofNullable(UserMapper.toDto(save))
        }
    }

    override fun update(userDto: UserDto): Optional<UserDto> {
        val original: Optional<User> = userRepository.findById(userDto.id!!)
        return if (original.isPresent) {
            val result = userRepository.save(UserMapper.fromDto(userDto))
            Optional.ofNullable(UserMapper.toDto(result))
        } else {
            Optional.empty()
        }
    }

    override fun delete(id: UUID) {
        userRepository.findById(id).ifPresent{ userRepository.delete(it) }
    }

    override fun listAll(): List<UserDto> {
        return userRepository.findAll().map { UserMapper.toDto(it) }
    }

    override fun getUserByEmail(email: String): UserDto {
        return UserMapper.toDto(userRepository.findByEmail(email.trim()))
    }

    override fun getUserByNick(nick: String): UserDto {
        return UserMapper.toDto(userRepository.findByNick(nick.trim()))
    }

}
