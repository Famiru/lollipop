package pl.mineland.lollipop.user.attribute

interface Attributes {
    fun findByLevel(level: Int): Attributes
    fun findByDescription(description: String): Attributes
}

enum class Strength(val level: Int, val description: String) {
    POTATO(1, "Standing on your feet is a challenge for you"),
    WORM(2, "Holding a fork in your hand is a challenge for you"),
    MOLE(3, "Carrying a backpack is a challenge for you"),
    EASY(4, "Feel the lack of strength but it is not bad"),
    NORMAL(5, "Average"),
    HUMAN(6, "Move a few cans of cola is not a challenge for you"),
    HARD(7, "You can carry heavy cardboard boxes, bravo"),
    MUTANT(8, "You jump like Michael Jordan"),
    CYBORG(9, "Mountain climbing with huge luggage is a pimple"),
    GOD(10, "Your blows make holes in the walls");

    companion object {
        fun getByLevel(level: Int): Strength? = values().find { it.level == level }
        fun getByLevelString(level: String): Strength? = values().find { it.level.dec() == level.toInt() }
    }
}

enum class Perception(val level: Int, val description: String) {
    POTATO(1, ""),
    WORM(2, ""),
    MOLE(3, ""),
    EASY(4, ""),
    NORMAL(5, ""),
    HUMAN(6, ""),
    HARD(7, ""),
    MUTANT(8, ""),
    CYBORG(9, ""),
    GOD(10, "");

    companion object {
        fun getByLevel(level: Int): Perception? = values().find { it.level == level }
        fun getByLevelString(level: String): Perception? = values().find { it.level.dec() == level.toInt() }
    }
}

enum class Endurance(val level: Int, val description: String) {
    POTATO(1, ""),
    WORM(2, ""),
    MOLE(3, ""),
    EASY(4, ""),
    NORMAL(5, ""),
    HUMAN(6, ""),
    HARD(7, ""),
    MUTANT(8, ""),
    CYBORG(9, ""),
    GOD(10, "");

    companion object {
        fun getByLevel(level: Int): Endurance? = values().find { it.level == level }
        fun getByLevelString(level: String): Endurance? = values().find { it.level.dec() == level.toInt() }
    }
}

enum class Charisma(val level: Int, val description: String) {
    POTATO(1, ""),
    WORM(2, ""),
    MOLE(3, ""),
    EASY(4, ""),
    NORMAL(5, ""),
    HUMAN(6, ""),
    HARD(7, ""),
    MUTANT(8, ""),
    CYBORG(9, ""),
    GOD(10, "");

    companion object {
        fun getByLevel(level: Int): Charisma? = values().find { it.level == level }
        fun getByLevelString(level: String): Charisma? = values().find { it.level.dec() == level.toInt() }
    }
}

enum class Intelligence(val level: Int, val description: String) {
    POTATO(1, ""),
    WORM(2, ""),
    MOLE(3, ""),
    EASY(4, ""),
    NORMAL(5, ""),
    HUMAN(6, ""),
    HARD(7, ""),
    MUTANT(8, ""),
    CYBORG(9, ""),
    GOD(10, "");

    companion object {
        fun getByLevel(level: Int): Intelligence? = values().find { it.level == level }
        fun getByLevelString(level: String): Intelligence? = values().find { it.level.dec() == level.toInt() }
    }
}

enum class Ability(val level: Int, val description: String) {
    POTATO(1, ""),
    WORM(2, ""),
    MOLE(3, ""),
    EASY(4, ""),
    NORMAL(5, ""),
    HUMAN(6, ""),
    HARD(7, ""),
    MUTANT(8, ""),
    CYBORG(9, ""),
    GOD(10, "");

    companion object {
        fun getByLevel(level: Int): Ability? = values().find { it.level == level }
        fun getByLevelString(level: String): Ability? = values().find { it.level.dec() == level.toInt() }
    }
}

enum class Luck(val level: Int, val description: String) {
    POTATO(1, ""),
    WORM(2, ""),
    MOLE(3, ""),
    EASY(4, ""),
    NORMAL(5, ""),
    HUMAN(6, ""),
    HARD(7, ""),
    MUTANT(8, ""),
    CYBORG(9, ""),
    GOD(10, "");

    companion object {
        fun getByLevel(level: Int): Luck? = values().find { it.level == level }
        fun getByLevelString(level: String): Luck? = values().find { it.level.dec() == level.toInt() }
    }
}
