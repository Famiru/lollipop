package pl.mineland.lollipop.user.attribute

class AttributeMapper {
    companion object {

        fun toEntity(attributeDto: AttributeDto): Attribute {
            return Attribute(attributeDto.strength.level,
                    attributeDto.perception.level,
                    attributeDto.endurance.level,
                    attributeDto.charisma.level,
                    attributeDto.intelligence.level,
                    attributeDto.ability.level,
                    attributeDto.luck.level,
                    attributeDto.attributePoints)
        }

        fun toDto(attribute: Attribute): AttributeDto {
            return valuesToDto(attribute.strength,
                    attribute.perception,
                    attribute.endurance,
                    attribute.charisma,
                    attribute.intelligence,
                    attribute.ability,
                    attribute.luck,
                    attribute.attributePoints)
        }

        fun entityToIntArray(attribute: Attribute): IntArray {
            return intArrayOf(attribute.strength, attribute.perception, attribute.endurance, attribute.charisma,
                    attribute.intelligence, attribute.ability, attribute.luck, attribute.attributePoints)
        }

        fun dtoToIntArray(attributeDto: AttributeDto): IntArray {
            return intArrayOf(attributeDto.strength.level, attributeDto.perception.level, attributeDto.endurance.level, attributeDto.charisma.level,
                    attributeDto.intelligence.level, attributeDto.ability.level, attributeDto.luck.level, attributeDto.attributePoints)
        }

        fun intArrayToEntity(intArray: IntArray): Attribute {
            val strength = intArray[0]
            val perception = intArray[1]
            val endurance = intArray[2]
            val charisma = intArray[3]
            val intelligence = intArray[4]
            val ability = intArray[5]
            val luck = intArray[6]
            val attributePoints = intArray[7]
            return Attribute(strength, perception, endurance, charisma, intelligence, ability, luck, attributePoints)
        }

        fun intArrayToDto(intArray: IntArray): AttributeDto {
            val strength = Strength.getByLevel(intArray[0]) ?: Strength.NORMAL
            val perception = Perception.getByLevel(intArray[1]) ?: Perception.NORMAL
            val endurance = Endurance.getByLevel(intArray[2]) ?: Endurance.NORMAL
            val charisma = Charisma.getByLevel(intArray[3]) ?: Charisma.NORMAL
            val intelligence = Intelligence.getByLevel(intArray[4]) ?: Intelligence.NORMAL
            val ability = Ability.getByLevel(intArray[5]) ?: Ability.NORMAL
            val luck = Luck.getByLevel(intArray[6]) ?: Luck.NORMAL
            val attributePoints = intArray[7]
            return AttributeDto(strength, perception, endurance, charisma, intelligence, ability, luck, attributePoints)
        }

        fun valuesToDto(strengthLevel: Int, perceptionLevel: Int, enduranceLevel: Int, charismaLevel: Int,
                        intelligenceLevel: Int, abilityLevel: Int, luckLevel: Int, attributePoints: Int): AttributeDto {
            val strength = Strength.getByLevel(strengthLevel) ?: Strength.NORMAL
            val perception = Perception.getByLevel(perceptionLevel) ?: Perception.NORMAL
            val endurance = Endurance.getByLevel(enduranceLevel) ?: Endurance.NORMAL
            val charisma = Charisma.getByLevel(charismaLevel) ?: Charisma.NORMAL
            val intelligence = Intelligence.getByLevel(intelligenceLevel) ?: Intelligence.NORMAL
            val ability = Ability.getByLevel(abilityLevel) ?: Ability.NORMAL
            val luck = Luck.getByLevel(luckLevel) ?: Luck.NORMAL
            return AttributeDto(strength, perception, endurance, charisma, intelligence, ability, luck, attributePoints)
        }

        fun valuesToEntity(strengthLevel: Int, perceptionLevel: Int, enduranceLevel: Int, charismaLevel: Int,
                           intelligenceLevel: Int, abilityLevel: Int, luckLevel: Int, attributePoints: Int): Attribute {
            return Attribute(strengthLevel, perceptionLevel, enduranceLevel, charismaLevel, intelligenceLevel, abilityLevel, luckLevel, attributePoints)
        }

        fun stringToList(text: String): List<Int> {
            return text.split(',').map { it.toInt() }.toList()
        }

        fun stringToAttributeDto(text: String): AttributeDto {
            val attributeArray = stringToList(text)
            if (attributeArray.size == 8) {
                return AttributeDto(Strength.getByLevel(attributeArray[0])!!,
                        Perception.getByLevel(attributeArray[1])!!,
                        Endurance.getByLevel(attributeArray[2])!!,
                        Charisma.getByLevel(attributeArray[3])!!,
                        Intelligence.getByLevel(attributeArray[4])!!,
                        Ability.getByLevel(attributeArray[5])!!,
                        Luck.getByLevel(attributeArray[6])!!,
                        attributePoints = attributeArray[7])
            }
            // TODO find some way to resolve it
            return AttributeDto(Strength.NORMAL, Perception.NORMAL, Endurance.NORMAL, Charisma.NORMAL, Intelligence.NORMAL, Ability.NORMAL, Luck.NORMAL, 5)
        }
    }
}
