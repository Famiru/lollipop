package pl.mineland.lollipop.user.attribute

class Attribute {
    var strength: Int
    var perception: Int
    var endurance: Int
    var charisma: Int
    var intelligence: Int
    var ability: Int
    var luck: Int
    var attributePoints: Int

    constructor(strength: Int,
                perception: Int,
                endurance: Int,
                charisma: Int,
                intelligence: Int,
                ability: Int,
                luck: Int,
                attributePoints: Int){
        this.strength = strength
        this.perception = perception
        this.endurance = endurance
        this.charisma = charisma
        this.intelligence = intelligence
        this.ability = ability
        this.luck = luck
        this.attributePoints = attributePoints
    }
}
