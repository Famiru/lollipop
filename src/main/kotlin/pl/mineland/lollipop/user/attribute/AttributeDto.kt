package pl.mineland.lollipop.user.attribute

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

@ApiModel(description = "Represents User.")
class AttributeDto {

    @ApiModelProperty(value = "Strength statistic.")
    var strength: Strength

    @ApiModelProperty(value = "Perception statistic.")
    var perception: Perception

    @ApiModelProperty(value = "Endurance statistic.")
    var endurance: Endurance

    @ApiModelProperty(value = "Charisma statistic.")
    var charisma: Charisma

    @ApiModelProperty(value = "Intelligence statistic.")
    var intelligence: Intelligence

    @ApiModelProperty(value = "Ability statistic.")
    var ability: Ability

    @ApiModelProperty(value = "Luck statistic.")
    var luck: Luck

    @ApiModelProperty(value = "Attribute Points.")
    var attributePoints: Int

    /**
     * Create instance of the AttributeDto object used for frontend communication.
     */
    @JsonCreator
    constructor(@JsonProperty("strength") strength: Strength,
                @JsonProperty("perception") perception: Perception,
                @JsonProperty("endurance") endurance: Endurance,
                @JsonProperty("charisma") charisma: Charisma,
                @JsonProperty("intelligence") intelligence: Intelligence,
                @JsonProperty("ability") ability: Ability,
                @JsonProperty("luck") luck: Luck,
                @JsonProperty("attribute_points") attributePoints: Int){
        this.strength = strength
        this.perception = perception
        this.endurance = endurance
        this.charisma = charisma
        this.intelligence = intelligence
        this.ability = ability
        this.luck = luck
        this.attributePoints = attributePoints
    }
}
