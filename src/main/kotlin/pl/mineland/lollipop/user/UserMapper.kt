package pl.mineland.lollipop.user

import pl.mineland.lollipop.user.attribute.AttributeMapper
import java.util.*

class UserMapper {

    companion object {
        fun fromDto(dto: UserDto): User {
            return User(dto.id ?: UUID.randomUUID(), dto.email, dto.nick, dto.password, dto.additionalDate, AttributeMapper.dtoToIntArray(dto.attributeDto).toString())
        }

        fun toDto(entity: User): UserDto {
            return UserDto(entity.id, entity.email, entity.nick, entity.password, entity.additionalDate, AttributeMapper.stringToAttributeDto(entity.attribute))
        }
    }
}
