package pl.mineland.lollipop.user

import org.hibernate.annotations.Type
import pl.mineland.lollipop.user.attribute.Attribute
import pl.mineland.lollipop.user.attribute.AttributeMapper
import java.time.LocalDate
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table
import javax.persistence.Column

@Entity
@Table(name = "parasite")
class User {

    @Id
    @Type(type = "pg-uuid")
    @Column(name = "id", unique = true)
    var id: UUID

    @Column(name="email", unique = true)
    val email: String

    @Column(name="nick", unique = true)
    val nick: String

    @Column(name="password", unique = false)
    val password: String

    @Column(name="additional_date", unique = false)
    val additionalDate: LocalDate

    @Column(name="attribute", unique = false)
    val attribute: String

    constructor(id: UUID, email: String, nick: String, password: String, additionalDate: LocalDate, attribute: String) {
        this.id = id
        this.email = email
        this.nick = nick
        this.password = password
        this.additionalDate = additionalDate
        this.attribute = attribute
    }

    constructor(email: String, nick: String, password: String) {
        this.id = UUID.randomUUID()
        this.email = email
        this.nick = nick
        this.password = password
        this.additionalDate = LocalDate.now()
        this.attribute = AttributeMapper.entityToIntArray(Attribute(5,5,5,5,5,5,5,5)).toString()
    }
}
