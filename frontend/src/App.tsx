import React from 'react'
import {BrowserRouter, Route} from 'react-router-dom'
import './css/style.css';
import './css/bootstrap.css'

import Header from './components/header'
import Footer from './components/footer'
import Home from "./components/home";
import Gallery from "./components/gallery";
import Upload from "./components/upload";
import Statistic from "./components/statistic";

const App: React.FC = () => {
    return (
        <BrowserRouter>
            <Header/>
            <div>
                <Route path="/home" render={() => <Home/>}/>
                <Route path="/gallery" render={() => <Gallery/>}/>
                <Route path="/upload" render={() => <Upload/>}/>
                <Route path="/statistic" render={() => <Statistic/>}/>
            </div>
            <Footer/>
        </BrowserRouter>
    );
};

export default App;
