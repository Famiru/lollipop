import React from 'react'

const Upload = () => {
    return (
        <div className="container container-fluid">
            <form>
                <div className="form-row align-items-center">
                    <div className="col-sm-5 my-1">
                        <input type="file" className="custom-file-input" id="customFile" />
                        <label className="custom-file-label" htmlFor="customFile">Choose file</label>
                    </div>

                    <div className="col-sm-5 my-1">
                        <label className="sr-only" htmlFor="inlineFormInputName">Name</label>
                        <input type="text" className="form-control" id="inlineFormInputName" placeholder="Select name" />
                    </div>

                    <div className="col-auto my-1">
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    );
};

export default Upload;
