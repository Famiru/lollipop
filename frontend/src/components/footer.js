import React from 'react'

const Footer = () => {
    return (
        <div className="footer">
            <p> Convert image to ASCII art <code>@copy 2019</code> </p>
        </div>
    )
};

export default Footer;
