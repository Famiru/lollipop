import React from 'react'
import logo from '../css/images/smallEye.png'

const Header = () => {
    return (
        <header className="header">
            <nav className="navbar navbar-expand-lg navbar-wrapper navbar-inverse navbar-brand">
                <ul className="navbar-nav">
                    <li className="nav-item active">
                        <a className="nav-link" href="/">
                            <img src={logo} alt="logo" width="30" height="30" className="d-inline-block align-top rightDistanced"/>
                            Home
                        </a>
                    </li>
                    <li className="nav-item active">
                        <a className="nav-link" href="./gallery">Gallery <span className="sr-only">(current)</span></a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="./upload">Upload</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="./statistic">Statistic</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link disabled" href="#" aria-disabled="true">VIP strophe</a>
                    </li>
                </ul>
            </nav>
        </header>
    )
};

export default Header;
