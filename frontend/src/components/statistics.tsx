import React, { useState, useEffect, useContext } from 'react'
import axios from 'axios';

type UserDto = {
    id: String,
    email: String,
    nick: String,
    password: String,
    additionalDate: String,
    attribute: Attribute
};

type Attribute = {
    strength: String,
    perception: String,
    endurance: String,
    charisma: String,
    intelligence: String,
    ability: String,
    luck: String,
    attribute_points: String
};

// function StatusDescription(props: UserDto): JSX.Element {
//     return {props};
// }

const Statistics = () => {
    let URL = 'http://localhost:8080';

    const [attribute, setAttribute] = useState( {
        strength: '',
        perception: '',
        endurance: '',
        charisma: '',
        intelligence: '',
        ability: '',
        luck: '',
        attribute_points: ''
    });

    const [user, setUser] = useState({
        id: '',
        email: '',
        nick: '',
        password: '',
        additionalDate: '',
        attribute: attribute
    });
    //
    // axios.get('http://localhost:8080/parasite',
    //     {id: '', email: '', nick: '', password: '', additionalDate: '',
    //         attribute: {
    //             strength: '', perception: '', endurance: '', charisma: '',
    //             intelligence: '', ability: '', luck: '', attribute_points: ''
    //         }
    //     },
    // );
    //
    // axios.get('http://localhost:8080/parasite',
    //     {id: '', email: '', nick: '', password: '', additionalDate: '',
    //         attribute: { strength: '', perception: '', endurance: '', charisma: '',
    //             intelligence: '', ability: '', luck: '', attribute_points: ''}},
    //     {headers: {'Content-Type': 'application/json'}})
    //     .then(function (response) {
    //         console.log(response);
    //     })
    //     .catch(function (error) {
    //         console.log(error);
    //     });

    return (
        <div>
            {/*<button onClick={() => }>Load users table</button>*/}
        </div>
    )
};
export default Statistics;
