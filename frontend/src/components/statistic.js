import React, { useState, useEffect, useContext } from 'react'
import axios from 'axios';

const Statistic = () => {

    const [user, setUser] = useState({
        id: '',
        email: '',
        nick: '',
        password: '',
        additionalDate: '',
        attribute: {
            strength: '',
            perception: '',
            endurance: '',
            charisma: '',
            intelligence: '',
            ability: '',
            luck: '',
            attribute_points: ''
        }
    });

    const loadUsers = () => {

        axios.get('http://localhost:8080/parasite',
            {
                params: {
                    id: '123e4567-e89b-12d3-a456-426655440001'
                }
            },
            {headers: {'Content-Type': 'application/json'}})
            .then(function (response) {
                setUser(response.data);
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    };

    return (
        <div>
            <button onClick={() => loadUsers()}>LOAD</button>
            <div>
                <table>
                    <tr>
                        <th>
                            {user.id}
                        </th>
                        <th>
                            {user.nick}
                        </th>
                        <th>
                            {user.email}
                        </th>
                        <th>
                            {user.additionalDate}
                        </th>
                        <th>
                            <tr>
                            {user.attribute.strength}
                            </tr>
                            <tr>
                            {user.attribute.endurance}
                            </tr>
                            <tr>
                            {user.attribute.perception}
                            </tr>
                            <tr>
                            {user.attribute.charisma}
                            </tr>
                            <tr>
                            {user.attribute.ability}
                            </tr>
                            <tr>
                            {user.attribute.intelligence}
                            </tr>
                            <tr>
                            {user.attribute.luck}
                            </tr>
                            <tr>
                            {user.attribute.attribute_points}
                            </tr>
                        </th>
                    </tr>
                </table>
            </div>
        </div>
    )
};

export default Statistic;
