import React from 'react'

const Gallery = (props) => {

    return (
        <div className="container">
            <div className="container-fluid">

                <form>
                    <div className="form-row align-items-center topDistanced">
                        <div className="col-sm-6 my-1">
                            <input type="file" className="custom-file-input" id="customFile" />
                            <label className="custom-file-label" htmlFor="customFile">Choose file</label>
                        </div>

                        <div className="col-sm-5 my-1">
                            <label className="sr-only" htmlFor="inlineFormInputName">Name</label>
                            <input type="text" className="form-control" id="inlineFormInputName" placeholder="Select name" />
                        </div>

                        <div className="col-auto my-1">
                            <button type="submit" className="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>

                <div className="row topDistanced">
                    <div className="col-md-7">
                        <a href="#">
                            <img className="img-fluid rounded mb-3 mb-md-0" src="http://placehold.it/700x300" alt=""/>
                        </a>
                    </div>
                    <div className="col-md-5">
                        <h3>Project One</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium veniam exercitationem expedita laborum at voluptate. Labore,
                            voluptates totam at aut nemo deserunt rem magni pariatur quos perspiciatis atque eveniet unde.</p>
                        <a className="btn btn-primary" href="#">View Project</a>
                    </div>
                </div>

                <div className="row topDistanced">
                    <div className="col-md-7">
                        <a href="#">
                            <img className="img-fluid rounded mb-3 mb-md-0" src="http://placehold.it/700x300" alt=""/>
                        </a>
                    </div>
                    <div className="col-md-5">
                        <h3>Project Two</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, odit velit cumque vero doloremque repellendus distinctio maiores
                            rem expedita a nam vitae modi quidem similique ducimus! Velit, esse totam tempore.</p>
                        <a className="btn btn-primary" href="#">View Project</a>
                    </div>
                </div>

                <div className="row topDistanced">
                    <div className="col-md-7">
                        <a href="#">
                            <img className="img-fluid rounded mb-3 mb-md-0" src="http://placehold.it/700x300" alt=""/>
                        </a>
                    </div>
                    <div className="col-md-5">
                        <h3>Project Two</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, odit velit cumque vero doloremque repellendus distinctio maiores
                            rem expedita a nam vitae modi quidem similique ducimus! Velit, esse totam tempore.</p>
                        <a className="btn btn-primary" href="#">View Project</a>
                    </div>
                </div>

            </div>
        </div>
    );
};

export default Gallery;
