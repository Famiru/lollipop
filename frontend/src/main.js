const { app, BrowserWindow } = require('electron');
app.on('ready', () => {
    let newWindows = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true
        }
    });
    // newWindows.loadFile('./public/index.html')
    newWindows.loadURL('http://localhost:3000/')

});